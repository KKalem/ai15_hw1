
public class StateScoreContainer {
	private int score;
	private GameState gs;
	
	public StateScoreContainer(int s, GameState g){
		score = s;
		gs = g;
	}
	
	public int getScore(){
		return score;
	}
	
	public GameState getState(){
		return gs;
	}
}
