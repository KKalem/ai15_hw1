import java.util.*;

public class Player {

	private static final boolean debug = false;
	private static final int timeStep = 150000;
	private static final int longTime = 500000000;
	private static final long noLimit = 900000000;

	private static boolean scoringAsRed;
	/**
	 * Performs a move
	 *
	 * @param pState
	 *            the current state of the board
	 * @param pDue
	 *            time before which we must have returned
	 * @return the next state the board is in after our move
	 */
	public GameState play(final GameState pState, final Deadline pDue) {

		Vector<GameState> lNextStates = new Vector<GameState>();
		pState.findPossibleMoves(lNextStates);

		if (lNextStates.size() == 0) {
			// Must play "pass" move if there are no other moves possible.
			return new GameState(pState, new Move());
		}

		/**
		 * Here you should write your algorithms to get the best next move, i.e.
		 * the best next state. This skeleton returns a random move instead.
		 */
		
		scoringAsRed = comparePiece(pState.getNextPlayer(), Constants.CELL_RED);
		final Deadline dl = new Deadline(Deadline.getCpuTime() + longTime);
		final int maxDepth = 10;
		
//		for(GameState ngs1 : lNextStates){
//			Vector<GameState> c = new Vector<GameState>();
//			ngs1.findPossibleMoves(c);
//			System.out.println("============ LV1 ===========\n" + ngs1.toString(0));
//			for(GameState ngs2 : c){
//				System.out.println("============ LV2 =============\n" + ngs2.toString(0) + "\n score: " + heuristic(ngs2, 2));
//				
//			}
//		}
//		System.out.println("================================\n==============================");
//		
//		
		return ab(pState, maxDepth, dl);
	}
	
	
//	private static int diagScore(GameState gs, int cellIndex){
//		int cellR = GameState.cellToRow(cellIndex);
//		int cellC = GameState.cellToCol(cellIndex);
//		
//		
//		int enemyDiag = 0;
//		
//		for(int r=-1; r < 2; r+=2){
//			for(int c=-1; c < 2; c+=2){
//				int diag = gs.get(cellR + r, cellC + c);
//				boolean diagIsRed = comparePiece(diag, Constants.CELL_RED);
//				
//				if(scoringAsRed && !diagIsRed) enemyDiag ++;
//				if(!scoringAsRed && diagIsRed) enemyDiag ++;
//				
//			}
//		}
//		
//		int enemyMult = -2;
//		
//		return enemyDiag*enemyMult;
//	}


	private static final int heuristic(final GameState gs, final int currentDepth){
		int score = 0;

		int reds = 0;
		int whites = 0;
		int redKings = 0;
		int whiteKings = 0;
		
		int diagScore = 0;

		final int selfPieceMultiplier = 10;
		final int enemyPieceMultiplier = 10;
		final int kingMultiplier = 0;
		

		if(gs.isRedWin() && scoringAsRed) return Integer.MAX_VALUE;
		if(gs.isWhiteWin() && !scoringAsRed) return Integer.MAX_VALUE;

		for(int i=0; i<32; i++){
			int cell = gs.get(i);
			if(comparePiece(cell, Constants.CELL_EMPTY)) continue;

			if(comparePiece(cell,Constants.CELL_RED)){
				reds++;
				if(comparePiece(cell,Constants.CELL_KING)) redKings ++;
			}
			if(comparePiece(cell,Constants.CELL_WHITE)){
				whites++;
				if(comparePiece(cell,Constants.CELL_KING)) whiteKings ++;
			}
//			diagScore += diagScore(gs, i);
		}
		score = - diagScore + (reds+redKings*kingMultiplier)*selfPieceMultiplier - (whites+whiteKings*kingMultiplier)*enemyPieceMultiplier;
		
		if(!scoringAsRed) score *= -1;

		debug("=========\n" + gs.toString(0) + "\nreds:" + reds + "\twhites:" + whites + "\nscoringAsRed? " + scoringAsRed + "\tscore:" + score + "\n=========");
		return score;
	}
	
	
	private static final GameState ab(final GameState gs, final int maxDepth, final Deadline dl){
		return max(gs, maxDepth, Integer.MIN_VALUE, Integer.MAX_VALUE, dl).getState();
	}
	

	private static final StateScoreContainer max(final GameState gs, final int depthLeft, int alf, final int bet, final Deadline dl){

		if(dl.timeUntil() <= timeStep || depthLeft <= 0) return new StateScoreContainer(heuristic(gs, depthLeft), gs);

		final Vector<GameState> children = new Vector<GameState>();
		gs.findPossibleMoves(children);

		if(children.size() == 0) return new StateScoreContainer(heuristic(gs, depthLeft), gs);

		int maxScore = Integer.MIN_VALUE;
		GameState bestChild = null;

		for(GameState child : children){
			StateScoreContainer n = min(child, depthLeft - 1, alf, bet, dl);
			int temp = n.getScore();
			
			if(temp >= maxScore){
				maxScore  = temp;
				bestChild = child;
			}

			alf = temp >= alf ? temp : alf;
			if(alf >= bet) return new StateScoreContainer(alf, bestChild);
		}

		return new StateScoreContainer(maxScore, bestChild);

	}

	private static final StateScoreContainer min(final GameState gs, final int depthLeft, final int alf, int bet, final Deadline dl){
		if(dl.timeUntil() <= timeStep || depthLeft <= 0) return new StateScoreContainer(heuristic(gs, depthLeft), gs);

		final Vector<GameState> children = new Vector<GameState>();
		gs.findPossibleMoves(children);

		if(children.size() == 0) return new StateScoreContainer(heuristic(gs, depthLeft), gs);

		int minScore = Integer.MAX_VALUE;
		GameState bestChild = null;
		
		for(GameState child : children){
			StateScoreContainer n = max(child, depthLeft - 1, alf, bet, dl);
			int temp = n.getScore();
			
			if(temp <= minScore){
				minScore  = temp;
				bestChild = child;
			}

			bet = temp <= bet ? temp : bet;
			if(alf >= bet) return new StateScoreContainer(bet, bestChild);
		}

		return new StateScoreContainer(minScore, bestChild);
	}




	// HELPER FUNCTIONS ====================================

	private static void debug(String s){
		if(debug) System.out.println(s);
	}

	private <E> String vecToStr(Vector<E> v){
		StringBuilder sb = new StringBuilder();


		sb.append("<");
		for(int i=0; i<v.size(); i++){
			sb.append(v.elementAt(i).toString() + ", ");
		}
		sb.append(">");

		return sb.toString();
	}

	private static final boolean comparePiece(int piece, int cellConst){
		int result = piece & cellConst;
		if(piece == 0 && cellConst == 0) return true;
		return result == 0 ? false : true;
	}


}
